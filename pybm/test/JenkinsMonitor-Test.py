import unittest
import requests
import time
import os
from pybm.JenkinsMonitor import JenkinsMonitor
from pybm.JenkinsMonitor import ParseException, UnknownProgressException
from pybm.Job import Status
from pybm.HttpClient import Response
from pybm.test.mocks.HttpClientMock import HttpClientMock

test_data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')

class Test(unittest.TestCase):
    def setUp(self):
        self.hcm = HttpClientMock()
        self.jm = JenkinsMonitor(self.hcm, 'http://url/', 'view')

    def testStatusTranslation(self):
        jm = JenkinsMonitor(HttpClientMock(), '','')
        self.assertEqual(Status.OK, jm.translate_state('blue'))
        self.assertEqual(Status.UNKNOWN, jm.translate_state('notbuilt'))
        self.assertEqual(Status.DISABLED, jm.translate_state('disabled'))
        self.assertEqual(Status.ERROR, jm.translate_state('red'))
        self.assertEqual(Status.UNKNOWN, jm.translate_state('aborted'))
        self.assertEqual(Status.OK, jm.translate_state('blue'))
        self.assertEqual(Status.BUILDING, jm.translate_state('_anime'))
        self.assertEqual(Status.UNKNOWN, jm.translate_state('balhblahbalh'))
        self.assertEqual(Status.WARNING, jm.translate_state('yellow'))

    def test2SuccessfullJobs(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'view')
        hcm.add_or_update_response('http://url/view/view/api/python', Response('{"jobs":[{"name":"test job 1","url":"someurl1","color":"red"}, {"name":"test job 2","url":"someurl2","color":"blue"}]}', 200))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/view/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('test job 1', list[0].name)
        self.assertEqual(Status.ERROR, list[0].status)
        self.assertEqual(100, list[0].percentage)
        self.assertEqual('test job 2', list[1].name)
        self.assertEqual(Status.OK, list[1].status)
        self.assertEqual(100, list[1].percentage)

    def testBuildingJob(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        hcm.add_or_update_response('http://url/view/All/api/python', Response('{"jobs":[{"name":"test job 1","url":"http://url/job/Job1","color":"blue_anime"}]}', 200))
        hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105, "timestamp":1388478776934}', 200))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/All/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('test job 1', list[0].name)
        self.assertEqual(Status.BUILDING, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def run_single_exception(self, hcm, jm, exception):
        hcm.add_or_update_response('http://url/view/All/api/python', None, exception)
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/All/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('Error getting view "All" from http://url/view/All/api/python', list[0].name)
        self.assertEqual(Status.ERROR, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def testHttpClientExceptions(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        self.run_single_exception(hcm, jm, Exception('testException'))
        self.run_single_exception(hcm, jm, requests.ConnectionError('testConnectionError'))
        self.run_single_exception(hcm, jm, requests.Timeout('testTimeout'))
        self.run_single_exception(hcm, jm, requests.HTTPError('testHTTPError'))
        self.run_single_exception(hcm, jm, requests.URLRequired('testURLRequired'))
        self.run_single_exception(hcm, jm, requests.TooManyRedirects('testTooManyRedirects'))
        self.run_single_exception(hcm, jm, requests.RequestException('testRequestException'))

    def testExcpetionGettingProgress1(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        hcm.add_or_update_response('http://url/view/All/api/python', Response('{"jobs":[{"name":"test job 1","url":"http://url/job/Job1","color":"blue_anime"}]}', 200))
        hcm.add_or_update_response('http://url/job/Job1/api/python', None, requests.ConnectionError('testConnectionError'))
        hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105, "timestamp":1388478776934}', 200))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/All/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('test job 1', list[0].name)
        self.assertEqual(Status.BUILDING, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def testExcpetionGettingProgress2(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        hcm.add_or_update_response('http://url/view/All/api/python', Response('{"jobs":[{"name":"test job 1","url":"http://url/job/Job1","color":"blue_anime"}]}', 200))
        hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        hcm.add_or_update_response('http://url/job/Job1/10/api/python', None, requests.ConnectionError('testConnectionError'))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/All/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('test job 1', list[0].name)
        self.assertEqual(Status.BUILDING, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def testParseExcpetion1(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        hcm.add_or_update_response('http://url/view/All/api/python', Response('  \n     <not parsable>', 200))
        hcm.add_or_update_response('http://url/job/Job1/api/python', None, requests.ConnectionError('testConnectionError'))
        hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105, "timestamp":1388478776934}', 200))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/view/All/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('Error getting view "All" from http://url/view/All/api/python', list[0].name)
        self.assertEqual(Status.ERROR, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def testParseExcpetion2(self):
        hcm = HttpClientMock()
        jm = JenkinsMonitor(hcm, 'http://url', 'All')
        hcm.add_or_update_response('http://url/view/All/api/python', Response('{"jobs":[{"name":"test job 1","url":"http://url/job/Job1","color":"blue_anime"}]}', 200))
        hcm.add_or_update_response('http://url/job/Job1/api/python', Response('  \n     <not parsable>', 200))
        hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105, "timestamp":1388478776934}', 200))
        hcm.add_or_update_response('http://url/queue/api/python', Response('{"items" : []}', 200))
        list = jm.get_jobs()
        self.assertEqual(1, hcm.get_amount_requested('http://url/job/Job1/api/python'))
        self.assertEqual(0, hcm.unknown_requests)
        self.assertEqual('test job 1', list[0].name)
        self.assertEqual(Status.BUILDING, list[0].status)
        self.assertEqual(100, list[0].percentage)

    def testProgressCalculation(self):
        jm = JenkinsMonitor(HttpClientMock(), '', '')
        timestamp = (time.time()*1000)-30000  # current time minus 30 seconds
        self.assertEqual(50, jm.calculate_progress(timestamp, 60000))  # 60000 = 60 seconds
        self.assertEqual(100, jm.calculate_progress(-1, 60000))  # -1 is given when Jenkins does not have n estimate

    def testParseResult(self):
        jm = JenkinsMonitor(HttpClientMock(), '', '')
        self.assertRaises(ParseException, jm.parse_result, '  \n     <not parsable>')

    def testRepairJenkinsBuildResponse(self):
        data_path = os.path.join(test_data_path, 'repairJenkinsBuildResponse')
        jm = JenkinsMonitor(HttpClientMock(), '', '')
        with open(os.path.join(data_path, 'full error.txt'), 'r') as response:
            data = response.read().replace('\n', '')
        repaired_data = jm.repair_erroneous_build_response(data)
        with open(os.path.join(data_path, 'no more error.txt'), 'r') as response:
            expected_data = response.read().replace('\n', '')
        self.assertEqual(repaired_data, expected_data)

    def testJobProgressGoodWeather(self):
        # Good weather
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        self.hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105, "timestamp":1388478776934}', 200))
        self.assertEqual(100,  self.jm.get_job_progress({"url": "http://url/job/Job1"}))
        self.assertEqual(1, self.hcm.get_amount_requested('http://url/job/Job1/api/python'))
        self.assertEqual(1, self.hcm.get_amount_requested('http://url/job/Job1/10/api/python'))

    def testJobProgressNoJobUrl(self):
        # No url for job
        self.assertRaisesRegexp(UnknownProgressException, 'No "url" in job.', self.jm.get_job_progress, {})

    def testJobProgressNoJobDetailResponse(self):
        # Failing to get a response for job details
        self.hcm.add_or_update_response('url1/api/python', None, requests.ConnectionError('testConnectionError'))
        self.assertRaisesRegexp(UnknownProgressException, 'could not get job details from url1/api/python', self.jm.get_job_progress, {"url": "url1"})

    def testJobProgressParseErrorJobDetails(self):
        # No parsable response for Job details
        self.hcm.add_or_update_response('url1/api/python', Response('<no> python "code"', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'could not parse job details from <no> python "code"', self.jm.get_job_progress, {"url": "url1"})

    def testJobProgressNoBuildDetailResponse(self):
        # Failing to get a response for build details
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        self.hcm.add_or_update_response('http://url/job/Job1/10/api/python', None, requests.ConnectionError('testConnectionError'))
        self.assertRaisesRegexp(UnknownProgressException, 'could not get build details from http://url/job/Job1/10/api/python', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testJobProgressNoBuildDetailResponse(self):
        # No parsable response for build details
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        self.hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('<no> python "code"', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'could not parse build details', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testJobProgressNoEstimatedDuration(self):
        # No estimatedDuration
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        self.hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"timestamp":1388478776934}', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'No "estimatedDuration" or "timestamp" in build details.', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testJobProgressNoTimestamp(self):
        # No timestamp
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"url":"http://url/job/Job1/10"}}', 200))
        self.hcm.add_or_update_response('http://url/job/Job1/10/api/python', Response('{"estimatedDuration":180105}', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'No "estimatedDuration" or "timestamp" in build details.', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testJobProgressNoLastBuild(self):
        #No "lastBuild" in job
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lasld":{"url":"http://url/job/Job1/10"}}', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'No "lastBuild" in job.', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testJobProgressNoUrlInLastBuild(self):
        #No "url" in job["lastBuild"]
        self.hcm.add_or_update_response('http://url/job/Job1/api/python', Response('{"lastBuild":{"bleh":"http://url/job/Job1/10"}}', 200))
        self.assertRaisesRegexp(UnknownProgressException, 'No "url" in job"lastBuild".', self.jm.get_job_progress, {"url": "http://url/job/Job1"})

    def testGetQueue(self):
        data_path = os.path.join(test_data_path, 'queues')
        with open(os.path.join(data_path, 'queue.txt'), 'r') as temp_data:
            data = temp_data.read().replace('\n', '')
        self.hcm.add_or_update_response('http://url//queue/api/python', Response((data), 200))
        queue_data = self.jm.get_queue()
        self.assertEqual(1, self.hcm.get_amount_requested('http://url//queue/api/python'))
        self.assertEqual(1, queue_data['pyDomotics-build-master'])
        self.assertEqual(2, queue_data['pyDomotics-build-DBLogingModule'])