
from pybm.HttpClient import Response

class HttpClientMock:
    def __init__(self):
        self.responses = {}
        self.unknown_requests = 0


    def get(self, request):
        if request.url in self.responses:
            self.responses[request.url]['times_requested'] += 1
            if self.responses[request.url]['exception'] is not None:
                raise self.responses[request.url]['exception']
            return self.responses[request.url]['response']
        else:
            print 'No response defined for: ' + request.url
            self.unknown_requests += 1
            return Response('', 404)

    def add_or_update_response(self, url, response, exception=None):
        self.responses[url] = {'response': response, 'exception': exception, 'times_requested': 0}

    def get_amount_requested(self, url):
        return self.responses[url]['times_requested']